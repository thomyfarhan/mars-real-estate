package com.aesthomic.marsrealestate.overview

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.aesthomic.marsrealestate.databinding.GridItemMarsBinding
import com.aesthomic.marsrealestate.domain.PropertyMars

class OverviewAdapter(private val clickListener: OverviewListener):
    ListAdapter<PropertyMars, OverviewAdapter.OverviewViewHolder>(OverviewDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OverviewViewHolder {
        return OverviewViewHolder.from(parent)
    }

    override fun onBindViewHolder(holder: OverviewViewHolder, position: Int) {
        holder.bind(getItem(position), clickListener)
    }

    class OverviewViewHolder(private val binding: GridItemMarsBinding):
        RecyclerView.ViewHolder(binding.root) {

        companion object {
            fun from(parent: ViewGroup): OverviewViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = GridItemMarsBinding
                    .inflate(layoutInflater, parent, false)

                return OverviewViewHolder(binding)
            }
        }

        fun bind(mars: PropertyMars, clickListener: OverviewListener) {
            binding.mars = mars
            binding.clickListener = clickListener
            binding.executePendingBindings()
        }
    }


}

class OverviewDiffCallback: DiffUtil.ItemCallback<PropertyMars>() {
    override fun areItemsTheSame(oldItem: PropertyMars, newItem: PropertyMars): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: PropertyMars, newItem: PropertyMars): Boolean {
        return oldItem == newItem
    }

}

interface OverviewListener {
    fun onClick(mars: PropertyMars)
}