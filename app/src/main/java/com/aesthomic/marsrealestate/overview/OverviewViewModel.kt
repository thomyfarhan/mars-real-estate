package com.aesthomic.marsrealestate.overview

import android.app.Application
import androidx.lifecycle.*
import com.aesthomic.marsrealestate.database.MarsDatabase
import com.aesthomic.marsrealestate.domain.PropertyMars
import com.aesthomic.marsrealestate.network.MarsApiFilter
import com.aesthomic.marsrealestate.repository.MarsRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

enum class MarsApiStatus {LOADING, ERROR, DONE}

class OverviewViewModel(app: Application): AndroidViewModel(app) {

    private val viewModelJob = Job()
    private val uiScope = CoroutineScope(viewModelJob + Dispatchers.Main)

    private val database = MarsDatabase.getInstance(app)
    private val repository = MarsRepository(database)

    private val _status = MutableLiveData<MarsApiStatus>()
    val status: LiveData<MarsApiStatus>
        get() = _status

    private val chosenFilter = MutableLiveData<MarsApiFilter>()

    val marsProperties = Transformations.map(chosenFilter) {
        when (it) {
            MarsApiFilter.SHOW_ALL -> repository.getMars()
            else -> repository.getMarsByType(it)
        }
    }

    private val _navigateToDetail = MutableLiveData<PropertyMars>()
    val navigateToDetail: LiveData<PropertyMars>
        get() = _navigateToDetail

    init {
        updateFilter(MarsApiFilter.SHOW_ALL)
    }

    private fun refreshMars() {
        uiScope.launch {
            try {
                _status.value = MarsApiStatus.LOADING
                val listResult = repository.getMarsFromNetwork()
                repository.refreshDatabaseMars(listResult)
                _status.value = MarsApiStatus.DONE
            } catch(e: Exception) {
                _status.value = MarsApiStatus.ERROR
            }
        }
    }

    fun updateFilter(filter: MarsApiFilter) {
        refreshMars()
        chosenFilter.value = filter
    }

    fun displayDetail(mars: PropertyMars) {
        _navigateToDetail.value = mars
    }

    fun displayDetailComplete() {
        _navigateToDetail.value = null
    }

    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }

    class Factory(private val app: Application): ViewModelProvider.Factory {
        @Suppress("UNCHECKED_CAST")
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(OverviewViewModel::class.java)) {
                return OverviewViewModel(app) as T
            }
            throw IllegalArgumentException("Unable to construct view model")
        }

    }
}
