package com.aesthomic.marsrealestate.overview


import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController

import com.aesthomic.marsrealestate.R
import com.aesthomic.marsrealestate.databinding.FragmentOverviewBinding
import com.aesthomic.marsrealestate.domain.PropertyMars
import com.aesthomic.marsrealestate.network.MarsApiFilter

class OverviewFragment : Fragment() {

    private lateinit var viewModel: OverviewViewModel
    private lateinit var binding: FragmentOverviewBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(layoutInflater,
            R.layout.fragment_overview, container, false)

        setHasOptionsMenu(true)
        initViewModel()
        initRecyclerView()
        viewModelObserver()

        return binding.root
    }

    private fun viewModelObserver() {
        viewModel.navigateToDetail.observe(this, Observer {
            it?.let {
                this.findNavController().navigate(
                    OverviewFragmentDirections.actionOverviewDestinationToDetailDestination(it))
                viewModel.displayDetailComplete()
            }
        })
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu_overview, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        viewModel.updateFilter(
            when(item.itemId) {
                R.id.item_overview_buy -> MarsApiFilter.SHOW_BUY
                R.id.item_overview_rent -> MarsApiFilter.SHOW_RENT
                else -> MarsApiFilter.SHOW_ALL
            }
        )
        return super.onOptionsItemSelected(item)
    }

    private fun initViewModel() {
        val app = requireNotNull(activity).application
        viewModel = ViewModelProviders.of(this, OverviewViewModel.Factory(app))
            .get(OverviewViewModel::class.java)

        binding.viewModel = viewModel
        binding.lifecycleOwner = this
    }

    private fun initRecyclerView() {
        binding.rvOverviewMars.adapter = OverviewAdapter(object: OverviewListener {
            override fun onClick(mars: PropertyMars) {
                viewModel.displayDetail(mars)
            }
        })
    }


}
