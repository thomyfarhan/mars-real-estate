package com.aesthomic.marsrealestate.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = [DatabasePropertyMars::class], version = 1)
abstract class MarsDatabase: RoomDatabase() {
    abstract val marsDao: MarsDao

    companion object {
        @Volatile
        private var INSTANCE: MarsDatabase? = null

        fun getInstance(context: Context): MarsDatabase {
            synchronized(this) {
                var instance = INSTANCE

                if (instance == null) {
                    instance = Room.databaseBuilder(
                        context,
                        MarsDatabase::class.java,
                        "mars_database"
                    ).build()

                    INSTANCE = instance
                }

                return instance
            }
        }
    }


}
