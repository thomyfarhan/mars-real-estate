package com.aesthomic.marsrealestate.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.aesthomic.marsrealestate.domain.PropertyMars

@Entity(tableName = "property_mars_table")
data class DatabasePropertyMars(

    @PrimaryKey
    val id: String,

    @ColumnInfo(name = "image_src")
    val imgSrcUrl: String,

    @ColumnInfo(name = "type")
    val type: String,

    @ColumnInfo(name = "price")
    val price: Double
)

fun List<DatabasePropertyMars>.asDomainModel(): List<PropertyMars> {
    return map {
        PropertyMars(
            id = it.id,
            imgSrcUrl =  it.imgSrcUrl,
            type = it.type,
            price = it.price
        )
    }
}