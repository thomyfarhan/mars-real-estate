package com.aesthomic.marsrealestate.database

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface MarsDao {

    @Query("SELECT * FROM property_mars_table ORDER BY id ASC")
    fun getAllProperties(): LiveData<List<DatabasePropertyMars>>

    @Query("SELECT * FROM property_mars_table WHERE type = :type ORDER BY id ASC")
    fun getPropertiesByType(type: String): LiveData<List<DatabasePropertyMars>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(vararg propertyMars: DatabasePropertyMars)
}