package com.aesthomic.marsrealestate.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.aesthomic.marsrealestate.database.DatabasePropertyMars
import com.aesthomic.marsrealestate.database.MarsDatabase
import com.aesthomic.marsrealestate.database.asDomainModel
import com.aesthomic.marsrealestate.domain.PropertyMars
import com.aesthomic.marsrealestate.network.MarsApi
import com.aesthomic.marsrealestate.network.MarsApiFilter
import com.aesthomic.marsrealestate.network.asDatabaseModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class MarsRepository(private val database: MarsDatabase) {


    fun getMarsByType(filter: MarsApiFilter): LiveData<List<PropertyMars>> {
        return Transformations.map(
            database.marsDao.getPropertiesByType(filter.value)) {

            it.asDomainModel()
        }
    }

    fun getMars(): LiveData<List<PropertyMars>> {
        return Transformations.map(
            database.marsDao.getAllProperties()) {

            it.asDomainModel()
        }
    }

    suspend fun refreshDatabaseMars(array: Array<DatabasePropertyMars>) {
        withContext(Dispatchers.IO) {
            database.marsDao.insertAll(*array)
        }
    }

    suspend fun getMarsFromNetwork(): Array<DatabasePropertyMars> {
        return MarsApi.retrofitService.getProperties().await().asDatabaseModel().toTypedArray()
    }
}