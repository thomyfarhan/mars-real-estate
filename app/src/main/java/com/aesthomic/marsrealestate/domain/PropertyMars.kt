package com.aesthomic.marsrealestate.domain

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class PropertyMars(
    val id: String,
    val imgSrcUrl: String,
    val type: String,
    val price: Double): Parcelable {

    val isRental: Boolean
        get() = type == "rent"
}
