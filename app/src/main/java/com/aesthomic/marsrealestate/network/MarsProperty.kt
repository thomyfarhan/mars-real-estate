package com.aesthomic.marsrealestate.network

import android.os.Parcelable
import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize

@Parcelize
data class MarsProperty(
    var id: String = "",
    @Json(name = "img_src")
    var imgSrcUrl: String = "",
    var type: String = "",
    var price: Double = 0.0): Parcelable {

    val isRental
        get() = type == "rent"
}