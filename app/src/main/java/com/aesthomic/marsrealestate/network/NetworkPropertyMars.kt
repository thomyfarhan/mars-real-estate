package com.aesthomic.marsrealestate.network

import com.aesthomic.marsrealestate.database.DatabasePropertyMars
import com.aesthomic.marsrealestate.domain.PropertyMars
import com.squareup.moshi.Json

data class NetworkPropertyMars(
    val id: String,

    @Json(name = "img_src")
    val imgSrc: String,

    val price: Double,
    val type: String
)

fun List<NetworkPropertyMars>.asDomainModel(): List<PropertyMars> {
    return map {
        PropertyMars(
            id = it.id,
            imgSrcUrl = it.imgSrc,
            type = it.type,
            price = it.price
        )
    }
}

fun List<NetworkPropertyMars>.asDatabaseModel(): List<DatabasePropertyMars> {
    return map {
        DatabasePropertyMars(
            id = it.id,
            imgSrcUrl = it.imgSrc,
            type = it.type,
            price = it.price
        )
    }
}