package com.aesthomic.marsrealestate.detail

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.aesthomic.marsrealestate.R
import com.aesthomic.marsrealestate.domain.PropertyMars

class DetailViewModel(marsProperty: PropertyMars, app: Application):
    AndroidViewModel(app) {

    private val _selectedMars = MutableLiveData<PropertyMars>()
    val selectedMars: LiveData<PropertyMars>
        get() = _selectedMars

    init {
        _selectedMars.value = marsProperty
    }

    val marsType = Transformations.map(selectedMars) {
        app.applicationContext.getString( R.string.display_type,
            app.applicationContext.getString( when(it.isRental) {
                true -> R.string.type_rent
                false -> R.string.type_sale
            })
        )
    }

    val marsPrice = Transformations.map(selectedMars) {
        app.applicationContext.getString(
            when(it.isRental) {
                true -> R.string.display_price_monthly_rental
                false -> R.string.display_price
            }, it.price
        )
    }
}
