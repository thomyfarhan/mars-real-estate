package com.aesthomic.marsrealestate.detail


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders

import com.aesthomic.marsrealestate.R
import com.aesthomic.marsrealestate.databinding.FragmentDetailBinding

class DetailFragment : Fragment() {

    private lateinit var binding: FragmentDetailBinding
    private lateinit var viewModel: DetailViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater,
            R.layout.fragment_detail, container, false)

        initViewModel()

        return binding.root
    }

    private fun initViewModel() {
        val marsProperty = DetailFragmentArgs.fromBundle(arguments!!).marsProperty
        val app = requireNotNull(this.activity).application

        val viewModelFactory = DetailViewModelFactory(marsProperty, app)
        viewModel = ViewModelProviders.of(this, viewModelFactory)
            .get(DetailViewModel::class.java)

        binding.viewModel = viewModel
        binding.lifecycleOwner = this
    }


}
