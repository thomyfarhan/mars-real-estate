package com.aesthomic.marsrealestate.detail

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.aesthomic.marsrealestate.domain.PropertyMars

class DetailViewModelFactory(private val marsProperty: PropertyMars,
                             private val app: Application):
    ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(DetailViewModel::class.java)){
            return DetailViewModel(marsProperty, app) as T
        }
        throw IllegalArgumentException("Unknown ViewModel Class")
    }


}