package com.aesthomic.marsrealestate

import android.view.View
import android.widget.ImageView
import androidx.core.net.toUri
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.aesthomic.marsrealestate.domain.PropertyMars
import com.aesthomic.marsrealestate.overview.MarsApiStatus
import com.aesthomic.marsrealestate.overview.OverviewAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions

@BindingAdapter("imageUrl")
fun bindImage(imageView: ImageView, imageUrl: String?) {
    imageUrl?.let {
        val imageUri = imageUrl.toUri().buildUpon().scheme("https").build()
        Glide.with(imageView.context)
            .load(imageUri)
            .apply(RequestOptions()
                .centerCrop()
                .placeholder(R.drawable.loading_animation)
                .error(R.drawable.ic_broken_image))
            .into(imageView)
    }
}

@BindingAdapter("listData")
fun bindRecyclerView(recyclerView: RecyclerView, list: List<PropertyMars>?) {
    val adapter = recyclerView.adapter as OverviewAdapter
    list?.let {
        adapter.submitList(list)
    }
}

@BindingAdapter("marsApiStatus")
fun bindStatus(imageView: ImageView, status: MarsApiStatus?) {
    when(status) {
        MarsApiStatus.LOADING -> {
            imageView.visibility = View.VISIBLE
            imageView.setImageResource(R.drawable.loading_animation)
        }
        MarsApiStatus.ERROR -> {
            imageView.visibility = View.VISIBLE
            imageView.setImageResource(R.drawable.ic_broken_image)
        }
        MarsApiStatus.DONE -> imageView.visibility = View.GONE
    }
}

@BindingAdapter("visibilityByStatus")
fun bindStatusVisibility(recyclerView: RecyclerView, status: MarsApiStatus?) {
    when(status) {
        MarsApiStatus.LOADING -> recyclerView.visibility = View.GONE
        else -> recyclerView.visibility = View.VISIBLE
    }
}