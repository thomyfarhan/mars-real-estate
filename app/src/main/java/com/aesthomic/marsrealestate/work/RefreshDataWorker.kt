package com.aesthomic.marsrealestate.work

import android.content.Context
import android.util.Log
import androidx.work.CoroutineWorker
import androidx.work.WorkerParameters
import com.aesthomic.marsrealestate.database.MarsDatabase
import com.aesthomic.marsrealestate.repository.MarsRepository
import retrofit2.HttpException

class RefreshDataWorker(context: Context, params: WorkerParameters)
    : CoroutineWorker(context, params) {

    companion object {
        const val WORK_NAME = "RefreshDataWorker"
    }

    override suspend fun doWork(): Result {
        val database = MarsDatabase.getInstance(applicationContext)
        val repository = MarsRepository(database)

        try {
            val listMars = repository.getMarsFromNetwork()
            repository.refreshDatabaseMars(listMars)
            Log.d("Worker", "Work request for sync is run")
        } catch (e: HttpException) {
            return Result.retry()
        }
        return Result.success()
    }
}